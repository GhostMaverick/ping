//
//  PingCD.swift
//  Ping
//
//  Created by Ghost Maverick on 6/28/15.
//  Copyright © 2015 Ember. All rights reserved.
//

import UIKit
import CoreData
import Foundation

let AppDelegateManagedObjectContext = AppDelegate().managedObjectContext

@objc(PingRecipientCD)
class PingRecipientCD: NSManagedObject
{
    @NSManaged var name: String?
    @NSManaged var phoneNumber: String?
}
