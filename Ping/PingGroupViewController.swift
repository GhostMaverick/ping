//
//  ViewController.swift
//  Ping
//
//  Created by Meikell Lamarr on 6/26/15.
//  Copyright (c) 2015 Ember. All rights reserved.
//

import UIKit
import CoreData

class PingGroupViewController: UIViewController
{
    var fetchedResultsController: NSFetchedResultsController?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendMessageButton: UIButton!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //
        do { try self.fetchRecipientsFromCoreData().performFetch() }
        catch let error as NSError { print(error.localizedDescription) }
        
        //
        self.sendMessageButton.layer.cornerRadius = 6.0
        self.sendMessageButton.layer.borderWidth = 1.0
        self.sendMessageButton.layer.borderColor = UIColor.whiteColor().CGColor
        self.sendMessageButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func fetchRecipientsFromCoreData() -> NSFetchedResultsController
    {
        if (self.fetchedResultsController != nil) {
            return self.fetchedResultsController!
        }
        let fetchRequest: NSFetchRequest = NSFetchRequest(entityName: "PingRecipientCD")
        fetchRequest.fetchBatchSize = 30
        let sortDescriptor1: NSSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor1]
        let resultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: AppDelegateManagedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController = resultsController
        self.fetchedResultsController?.delegate = self
        return self.fetchedResultsController!
    }

    
    @IBAction func addContactToTableView(sender: UIButton) -> Void
    {
        guard self.nameTextField.text != "" else {
            print("No Name Entered")
            return
        }
        
        guard self.phoneNumberTextField.text != "" else {
            print("No Phone Number entered")
            return
        }
        
        let entity = NSEntityDescription.entityForName("PingRecipientCD", inManagedObjectContext: AppDelegateManagedObjectContext)
        let recipient = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: AppDelegateManagedObjectContext)
        recipient.setValue(self.nameTextField.text!, forKey: "name")
        recipient.setValue(self.phoneNumberTextField.text!, forKey: "phoneNumber")
        
        do { try AppDelegateManagedObjectContext.save() }
        catch let error as NSError { print("Could not save in \(__FUNCTION__). Error: \(error.localizedDescription)") }
        
        self.nameTextField.text = ""
        self.phoneNumberTextField.text = ""
    }
    
    @IBAction func dismissiOSKeyboard(sender: UITapGestureRecognizer) -> Void
    {
        self.view.endEditing(true)
    }
    
    @IBAction func prepareToSendSMS(sender: UIButton) -> Void
    {
        print("Preparing to send SMS...")
        let fetchRequest = NSFetchRequest(entityName: "PingRecipientCD")
        do {
            let fetchedResults = try AppDelegateManagedObjectContext.executeFetchRequest(fetchRequest) as? [PingRecipientCD]
            var smsRecipients = [String]()
            for result in fetchedResults! {
                smsRecipients.append(result.phoneNumber!)
            }
            NSNotificationCenter.defaultCenter().postNotificationName(kOkayToUseSMSRecipientsNotification, object: self, userInfo: ["Recipients":smsRecipients])
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}


extension PingGroupViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return self.fetchedResultsController!.sections!.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.fetchedResultsController!.sections![section].numberOfObjects
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let title = UILabel()
        title.backgroundColor = UIColor.clearColor()
        title.text = "Message Group"
        title.textColor = UIColor.whiteColor()
        title.textAlignment = NSTextAlignment.Center
        return title
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "recipientCell")
        
        let info: PingRecipientCD = self.fetchedResultsController?.objectAtIndexPath(indexPath) as! PingRecipientCD
        
        cell.textLabel!.text = info.name
        cell.textLabel!.textColor = UIColor.whiteColor()
        cell.detailTextLabel!.text = info.phoneNumber
        cell.detailTextLabel!.textColor = UIColor.whiteColor()
        cell.backgroundColor = UIColor.clearColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if editingStyle == .Delete
        {
            let recipient: PingRecipientCD = self.fetchedResultsController?.objectAtIndexPath(indexPath) as! PingRecipientCD
            AppDelegateManagedObjectContext.deleteObject(recipient)
            do { try AppDelegateManagedObjectContext.save() }
            catch { print("Error saving in \(__FUNCTION__)")}
        }
        else if editingStyle == .Insert
        {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath)
    {
    
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
}

extension PingGroupViewController: NSFetchedResultsControllerDelegate
{
    func controllerWillChangeContent(controller: NSFetchedResultsController)
    {
        self.tableView.beginUpdates()
    }
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: NSManagedObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?)
    {
        switch type
        {
        case .Insert:
            if let newIndexPath = newIndexPath {
                self.tableView.insertRowsAtIndexPaths([newIndexPath],
                    withRowAnimation:UITableViewRowAnimation.Fade)
            }
        case .Delete:
            if let indexPath = indexPath {
                self.tableView.deleteRowsAtIndexPaths([indexPath],
                    withRowAnimation: UITableViewRowAnimation.Fade)
            }
        case .Update:
            break
        case .Move:
            if let indexPath = indexPath {
                if let newIndexPath = newIndexPath {
                    self.tableView.deleteRowsAtIndexPaths([indexPath],
                        withRowAnimation: UITableViewRowAnimation.Fade)
                    self.tableView.insertRowsAtIndexPaths([newIndexPath],
                        withRowAnimation: UITableViewRowAnimation.Fade)
                }
            }
        }
    }
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType)
    {
        switch type
        {
        case .Insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex),
                withRowAnimation: UITableViewRowAnimation.Fade)
            
        case .Delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex),
                withRowAnimation: UITableViewRowAnimation.Fade)
        default:
            break
        }
    }
    func controllerDidChangeContent(controller: NSFetchedResultsController)
    {
        self.tableView.endUpdates()
    }
}
