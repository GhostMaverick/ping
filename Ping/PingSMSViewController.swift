//
//  PingSMSViewController.swift
//  Ping
//
//  Created by Ghost Maverick on 6/28/15.
//  Copyright © 2015 Ember. All rights reserved.
//

import UIKit
import MessageUI

let kOkayToUseSMSRecipientsNotification = "kOkayToUseSMSRecipientsNotification"
let kOkayToSendSMSNotification = "kOkayToSendSMSNotification"


class PingSMSViewController: UIViewController
{
    var recipients: [String]?
    var timer: NSTimer!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Observe for the sms message queue notification
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setSMSRecipients:", name: kOkayToUseSMSRecipientsNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "queueSMSRecipients:", name: kOkayToSendSMSNotification, object: nil)
        
        // 
        self.textView.layer.cornerRadius = 10.0
        self.textView.layer.borderWidth = 3.0
        self.textView.layer.borderColor = UIColor.whiteColor().CGColor
        self.sendButton.layer.cornerRadius = 6.0
        self.sendButton.layer.borderWidth = 1.0
        self.sendButton.layer.borderColor = UIColor.whiteColor().CGColor
        self.sendButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.cancelButton.layer.cornerRadius = 6.0
        self.cancelButton.layer.borderWidth = 1.0
        self.cancelButton.layer.borderColor = UIColor.whiteColor().CGColor
        self.cancelButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: -
    func setSMSRecipients(sender: NSNotification) -> Void
    {
        let userInfo = sender.userInfo as! [String:[String]]
        self.recipients = userInfo["Recipients"]
    }
    
    func queueSMSRecipients(sender: NSNotification) -> Void
    {
        self.timer.invalidate()
        self.timer = nil
        self.recipients!.removeAtIndex(0)
        self.sendSMS(nil)
    }
    
    // MARK: -
    func postOkayToSendSMSNotification(sender: NSTimer) -> Void
    {
        NSNotificationCenter.defaultCenter().postNotificationName(kOkayToSendSMSNotification, object: self)
    }
    
    // MARK: -
    @IBAction func dismissiOSKeyboard(sender: UITapGestureRecognizer) -> Void
    {
        self.view.endEditing(true)
    }
    
    // MARK: -
    @IBAction func sendSMS(sender: UIButton?) -> Void
    {
        // If there are no more recipients, or there are no recipients, so dismiss view controller
        if self.recipients!.count == 0 || self.recipients == nil
        {
            self.dismissViewControllerAnimated(true, completion: nil)
            return
        }
        
        if !MFMessageComposeViewController.canSendText() {
            // Can not send text message
            print("Cant send text message")
            return
        }
        
        let messageComposeController = MFMessageComposeViewController()
        messageComposeController.messageComposeDelegate = self
        messageComposeController.recipients = [self.recipients![0]]
        messageComposeController.body = self.textView.text!
        print("Sending Message to \(self.recipients![0])...")
        self.presentViewController(messageComposeController, animated: true, completion: nil)
    }
    
    // MARK: - 
    @IBAction func cancelSMS(sender: UIButton) -> Void
    {
        self.recipients = nil
        self.timer = nil
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}



extension PingSMSViewController: MFMessageComposeViewControllerDelegate
{
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult)
    {
        switch result.rawValue
        {
            case MessageComposeResultCancelled.rawValue:
                print("Cancelled")
            case MessageComposeResultFailed.rawValue:
                print("Failed")
            case MessageComposeResultSent.rawValue:
               print("Sent")
            default:
                break
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1.25, target: self, selector: "postOkayToSendSMSNotification:", userInfo: nil, repeats: false)
    }
}
